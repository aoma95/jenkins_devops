# jenkins_devops
[![Build Status](http://34.229.80.253:8080/buildStatus/icon?job=jenkins)](http://34.229.80.253:8080/job/jenkins/)

pipeline{
    agent any
    stages{
        stage('Deploy app on EC2-cloud Production') {
            steps{
                withCredentials([sshUserPrivateKey(credentialsId: "ssh_prod", keyFileVariable: 'keyfile', usernameVariable: 'NUSER')]) {
                    catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                        script{ 
                            sh'''
                                ssh -o StrictHostKeyChecking=no -i ${keyfile} ${NUSER}@3.82.252.247 -C \'sudo docker rm -f static-webapp-prod\'
                                ssh -o StrictHostKeyChecking=no -i ${keyfile} ${NUSER}@3.82.252.247 -C \'sudo docker run -d --name static-webapp-prod  -e PORT=80 -p 80:80 sadofrazer/alpinehelloworld\'
                            '''
                        }
                    }
                }
            }
        }
    }
}
